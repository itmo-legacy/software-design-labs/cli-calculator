{-# LANGUAGE OverloadedStrings #-}

module Expression (Expression(..), eval, printRPN) where

import Data.Text.Lazy (Text)
import qualified Data.Text.Lazy as T (pack)

infixl 1 :+
infixl 1 :-
infixl 2 :*
infixl 2 :/

data Expression = Expression :+ Expression
                | Expression :- Expression
                | Expression :* Expression
                | Expression :/ Expression
                | Neg Expression
                | Val Int
                deriving (Show, Eq)

eval :: Expression -> Int
eval (a :+ b) = eval a + eval b
eval (a :- b) = eval a - eval b
eval (a :* b) = eval a * eval b
eval (a :/ b) = eval a `div` eval b
eval (Neg a)  = -(eval a)
eval (Val x)  = x

printRPN :: Expression -> Text
printRPN (a :+ b) = binopRPN "+" a b
printRPN (a :- b) = binopRPN "-" a b
printRPN (a :* b) = binopRPN "*" a b
printRPN (a :/ b) = binopRPN "/" a b
printRPN (Neg a) = printRPN a <> " " <> "neg"
printRPN (Val x) = T.pack (show x)

binopRPN :: Text -> Expression -> Expression -> Text
binopRPN op a b = printRPN a <> " " <> printRPN b <> " " <> op
