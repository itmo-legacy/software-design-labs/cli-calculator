{-# LANGUAGE LambdaCase #-}

module Lib
    ( module Expression
    , parseExpression
    ) where

import qualified Data.ByteString.Char8 as BS (pack)
import Data.Function ((&))
import Data.Functor ((<&>))
import Text.Printf (printf)

import Expression (Expression(..), eval, printRPN)
import ExpressionParser (expressionP, runP)
import Lexer (tokenize)

parseExpression :: String -> Either String Expression
parseExpression s
  = BS.pack s
  & tokenize
  <&> runP expressionP
  >>= \case Just (expr, []) -> Right expr
            Just (expr, ts)
              -> Left (printf "'%s' is left unparsed (parsed: '%s')"
                       (show ts) (show expr))
            Nothing -> Left "parsing failed"
