{-# LANGUAGE OverloadedStrings #-}

module Main where

import Data.Either (either)
import Data.Foldable (for_)
import Data.Functor ((<&>))
import qualified Data.Text.Lazy.IO as T (putStrLn)

import Lib (eval, parseExpression, printRPN)

main :: IO ()
main = do
  parsedLines <- getContents <&> lines <&> map parseExpression
  for_ parsedLines $ either
    (\msg -> putStrLn $ "Oops! " <> msg)
    (\parsed -> do
        putStrLn ("result: " <> show (eval parsed))
        T.putStrLn ("rpn: " <> printRPN parsed)
    )
